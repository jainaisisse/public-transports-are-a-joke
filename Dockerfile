FROM node:19.7.0-alpine3.17

# Setup timezone
ENV TZ=Europe/Paris

RUN apk update \
    && apk upgrade \
    && apk add --no-cache tzdata \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

# Install dependencies
RUN npm ci

COPY . .

# Transpile Typescript and reinstall only production dependencies
RUN set -e \
    && npm run build \
    && npm ci --omit=dev \
    && chown -R node:node /usr/src/app

USER node

CMD npm start -- --cron
